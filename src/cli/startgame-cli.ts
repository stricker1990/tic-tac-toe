import { printField, getPlayerChoise, printGameState, startNewGame } from './dialogs';
import { Field } from '../game/Field';
import { GameState } from '../game/GameState';

export function startGame() {
  while (true) {

    const field = new Field();

    while (field.gameState() === GameState.GameNotFinished) {

      printField(field);

      let error = '';
      do {
        const playerChoice = getPlayerChoise();
        error = field.playerMove(playerChoice.row, playerChoice.column);
      }while (error !== '');

      printGameState(field.gameState());

    }
    if (!startNewGame()) {
      break;
    }
  }
}
