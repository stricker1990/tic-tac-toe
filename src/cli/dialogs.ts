import readlineSync from 'readline-sync';
import { Field } from '../game/Field';
import { FieldValue } from '../game/FieldValue';
import { PlayerType } from '../game/PlayerType';
import { GameState } from '../game/GameState';

export function getPlayerChoise() {
  console.log('Выберите клетку, в которую будете ходить: ');

  const row = readlineSync.questionInt('Строка: ') - 1;
  const column = readlineSync.questionInt('Колонка: ') - 1;

  return { row, column };
}

export function startNewGame() {
  return readlineSync.keyInYN('Начать новую игру?');
}

export function printField(field: Field) {

  let playerStr = 'Нолики';
  if (field.currentPlayer === PlayerType.Cross) {
    playerStr = 'Крестики';
  }

  console.log(`Ходят ${playerStr}`);
  for (let i = 0; i < Field.SIZE; i++) {
    printFieldRow(field, i);
  }
}

export function printGameState(state: GameState) {
  switch (state){
    case GameState.CrossWin:
      console.log('Крестики выиграли');
      break;
    case GameState.ZeroWin:
      console.log('Нолики выиграли');
      break;
    case GameState.NobodyWin:
      console.log('Ничья');
      break;
    case GameState.GameNotFinished:
      console.log('Игра продолжается');
      break;
  }
}

function printFieldRow(field: Field, row: number) {
  let result = '';
  for (let i = 0; i < Field.SIZE; i++) {
    result = result + fieldValueToString(field.getFiledValue(row, i));
  }
  console.log(result);
}

function fieldValueToString(fieldValue: FieldValue): string {
  switch (fieldValue) {
    case FieldValue.Cross:
      return 'X';
    case FieldValue.Zero:
      return '0';
  }
  return '-';
}
