import express from 'express';

import cors from 'cors';

import logger from '../util/logger';

import router from '../server/router';

export default function startServer() {

  const app = createServer();

  const port = 2000;

  app.listen(port, () => logger.log(`'Tic-tac-toe' app listening on port ${port}!`));
}

export function createServer() {
  const app = express();

  app.use(cors());

  app.use(express.json());

  app.use(router);

  return app;
}
