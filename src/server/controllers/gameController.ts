import { Field } from '../../game/Field';
import { FieldValue } from '../../game/FieldValue';
import { PlayerType } from '../../game/PlayerType';

let field = new Field();

export function reset(matrix?: FieldValue[][], currentPlayer?: PlayerType) {
  field = new Field(matrix, currentPlayer);
}

const controller = {
  field,
  reset,
};

export default controller;
