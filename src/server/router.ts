import express from 'express';

import logger from '../util/logger';

import gameController from '../server/controllers/gameController';

const router = express.Router();

router.get('/getField', (req, res) => {
  res.status(200).send(gameController.field.matrix);
});

router.post('/move', (req, res) => {
  const error = gameController.field.playerMove(req.body.y - 1, req.body.x - 1);
  if (error) {
    res.status(500).send(error);
    return;
  }
  res.status(200).send('ok');
});

export default router;
