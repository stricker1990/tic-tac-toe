import { FieldValue } from './FieldValue';
import { GameState } from './GameState';
import { PlayerType } from './PlayerType';

export class Field{
  static readonly SIZE = 3;

  matrix: FieldValue[][];

  currentPlayer: PlayerType;

  constructor(matrix?: FieldValue[][], currentPlayer: PlayerType = PlayerType.Zero) {
    const defValue = FieldValue.Empty;
    this.matrix = [
        [defValue, defValue, defValue],
        [defValue, defValue, defValue],
        [defValue, defValue, defValue],
    ];
    this.currentPlayer = PlayerType.Zero;

    if (matrix) {
      this.matrix = matrix;
    }
    this.currentPlayer = currentPlayer;
  }

  static getValue(player: PlayerType): FieldValue {
    if (player === PlayerType.Cross) {
      return FieldValue.Cross;
    }
    return FieldValue.Zero;
  }

  static parseFromString(str: string): Field {

    function parseRow(str: string): FieldValue[] {
      const result: FieldValue[] = [];
      for (const s of str) {
        if (FieldValue[+s]) {
          result.push(+s);
        }else {
          result.push(FieldValue.Empty);
        }
      }
      return result;
    }

    const matrix: number[][] = [];

    const rows = str.split('|');

    if (rows.length !== Field.SIZE) {
      return new Field();
    }

    for (const row of rows) {
      if (row.length !== Field.SIZE) {
        return new Field();
      }
      matrix.push(parseRow(row));
    }

    return new Field(matrix);
  }

  playerMove(row: number, column: number): string {
    if (this.matrix[row][column] !== FieldValue.Empty) {
      return `cell ${row}, ${column} is not empty`;
    }
    this.matrix[row][column] = Field.getValue(this.currentPlayer);
    this.nextPlayer();
    return '';
  }

  nextPlayer(): void {
    if (this.currentPlayer === PlayerType.Zero) {
      this.currentPlayer = PlayerType.Cross;
    }else {
      this.currentPlayer = PlayerType.Zero;
    }
  }

  gameState(): GameState {
    function getCountObject() {
      return {
        rows: Array(Field.SIZE).fill(0),
        columns: Array(Field.SIZE).fill(0),
        mainDiagonal: 0,
        sideDiagonal: 0,
      };
    }

    function isWinner(counts: any): boolean {
      const { rows, columns, mainDiagonal, sideDiagonal } = counts;
      return rows.includes(Field.SIZE)
      || columns.includes(Field.SIZE)
      || mainDiagonal === Field.SIZE
      || sideDiagonal === Field.SIZE;
    }

    const crossCounts = getCountObject();
    const zeroCounts = getCountObject();

    let emptyCellsCount: number = 0;

    for (let i = 0; i < Field.SIZE;  i++) {
      for (let j = 0; j < Field.SIZE; j++) {
        const value = this.matrix[i][j];
        if (value === FieldValue.Empty) {
          emptyCellsCount += 1;
          continue;
        }
        let counts = crossCounts;
        if (value === FieldValue.Zero) {
          counts = zeroCounts;
        }

        counts.rows[i] += 1;
        counts.columns[j] += 1;

        if (i === j) {
          counts.mainDiagonal += 1;
        }
        if (i === Field.SIZE - j - 1) {
          counts.sideDiagonal += 1;
        }
      }
    }

    if (emptyCellsCount === 0) {
      return GameState.NobodyWin;
    }

    if (isWinner(crossCounts) && isWinner(zeroCounts)) {
      return GameState.NobodyWin;
    }

    if (isWinner(crossCounts)) {
      return GameState.CrossWin;
    }

    if (isWinner(zeroCounts)) {
      return GameState.ZeroWin;
    }

    return GameState.GameNotFinished;
  }

  toString(): string {
    return this.matrix.map(row => {
      return row.map(column => `${column}`).join('');
    }).join('|');
  }

  getFiledValue(row: number, column: number): FieldValue {
    if (row >= Field.SIZE
      || column >= Field.SIZE
      || row < 0
      || column < 0) {
      return FieldValue.Empty;
    }
    return this.matrix[row][column];
  }
}
