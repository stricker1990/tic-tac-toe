export enum GameState {
    GameNotFinished = -1,
    NobodyWin = 0,
    CrossWin = 1,
    ZeroWin = 2,
}
