import { Given, Then } from 'cucumber';

import { Field } from '../../src/game/Field';
import { GameState } from '../../src/game/GameState';

import supertest from 'supertest';

import gameController from '../../src/server/controllers/gameController';

import { createServer } from '../../src/server';
import logger from '../../src/util/logger';

const app = createServer();

let error: string;

Given('пустое поле', () => {
  gameController.reset();
  if (gameController.field.toString() !== '000|000|000') {
    throw `current field: ${gameController.field.toString()} expected field: 000|000|000`;
  }
});

Given('поле {string}', (field: string) => {
  gameController.field = Field.parseFromString(field);
  if (gameController.field.toString() !== field) {
    throw `current field ${gameController.field.toString()} expected field: ${field}`;
  }
});

Given('ходит игрок {int}', (player: number) => {
  gameController.field.currentPlayer = player;
});

Then('переход хода к игроку {int}', (player: number) => {
  if (gameController.field.currentPlayer !== player) {
    throw `current player ${gameController.field.currentPlayer}. expected player ${player}`;
  }
});

Then('игрок ходит в клетку {int}, {int}', (row: number, column: number) => {
  error = '';
  return supertest(app)
  .post('/move')
  .send({ x: row, y: column })
  .then((res) => {
    if (res.status !== 200) {
      error = res.text;
    }
  });
});

Then('игра продолжается', () => {
  if (gameController.field.gameState() !== GameState.GameNotFinished) {
    throw `expected gameState ${GameState.GameNotFinished} current gameState ${gameController.field.gameState()}`;
  }
});

Then('поле становится {string}', (field: string) => {
  if (gameController.field.toString() !== field) {
    throw `current field ${gameController.field.toString()} expected field: ${field}. last error: ${error}`;
  }
});

Then('возвращается ошибка', () => {
  if (error === '') {
    throw `the player’s last move must contain an error. Current field: ${gameController.field}`;
  }
});

Then('победил игрок {int}', (player: number) => {
  const gameState = gameController.field.gameState();
  if (player !== gameState) {
    throw `expected gameState ${GameState[player]} current gameState ${GameState[gameState]}`;
  }
});

Then('ничья', () => {
  const gameState = gameController.field.gameState();
  if (gameState !== GameState.NobodyWin) {
    throw `${gameState}`;
  }
});
